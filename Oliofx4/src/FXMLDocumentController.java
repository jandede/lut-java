
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField textInput = new TextField();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	
    	textInput.textProperty().addListener((observable,oldValue,newValue) -> {
        	label.setText(newValue);
        });
    }    
    
}
