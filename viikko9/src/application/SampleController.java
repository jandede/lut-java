package application;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.awt.List;
import java.io.*;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

public class SampleController implements Initializable {
	@FXML
	ComboBox<String> comboTeatteri = new ComboBox();
	@FXML
	TextField fieldEsitysPaiva = new TextField();
	@FXML
	TextField fieldAAika = new TextField();
	@FXML
	TextField fieldPAika = new TextField();
	@FXML
	TextField fieldAnnaNimi = new TextField();
	@FXML
	Button buttonListaaElokuvat;
	@FXML
	Button buttonNimiHaku;
	@FXML
	ListView<String> listViewMovies = new ListView<>();

	TeatteriReader tr = TeatteriReader.getInstance();
	String selectedComboItem;

	@FXML
	private void handleButtonAction(ActionEvent event) {
		if (event.getSource() == buttonListaaElokuvat && selectedComboItem != null && !fieldEsitysPaiva.getText().isEmpty() && !fieldPAika.getText().isEmpty() && !fieldAAika.getText().isEmpty()) { //  && selectedComboItem != null
			fillListView(findMovies(Integer.parseInt(selectedComboItem.split(": ")[selectedComboItem.split(": ").length-1]), fieldEsitysPaiva.getText(), fieldAAika.getText(), fieldPAika.getText()));
			
		} else if (event.getSource() == buttonNimiHaku && !fieldAnnaNimi.getText().isEmpty()) {
			ArrayList<Movie> foundMovies = tr.findAMovie(fieldAnnaNimi.getText());
			try {
				fillListView(foundMovies, true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		try {
			//tr.getXML();
			fillCombo(tr.getXMLTheatre());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//fillCombo(tr.getArrList());

		//findMovies(1015);
		
		comboTeatteri.valueProperty().addListener((obs, oldValue, newValue) -> {
			selectedComboItem = newValue;
			// Suoltaa valitun combobox tavaran ID:n
			String sid = newValue.split(": ")[1];
			int id = Integer.parseInt(newValue.split(": ")[newValue.split(": ").length-1]);
	
			
			fillListView(findMovies(id));
		});

	}
	
	private void fillListView(ArrayList<Movie> alm){
		ObservableList<String> obsList = FXCollections.observableArrayList();
		for(Movie movie : alm) {
			obsList.add(movie.title + "\t" + movie.kloStart +" - "+movie.kloEnd);
		}
		listViewMovies.setItems(obsList);
	}
	private void fillListView(ArrayList<Movie> alm, boolean filterByTheatre) throws IOException{
		ArrayList<Finnkino> theatres = new ArrayList<Finnkino>();
		theatres = tr.getXMLTheatre();
		
		ObservableList<String> obsList = FXCollections.observableArrayList();
		String theatreName = "";
		for(Movie movie : alm) {
			for (Finnkino theatre : theatres) {
				if(theatre.id == movie.idOfTheatre) {
					theatreName = theatre.name;
					break;
				}
			}
			obsList.add(movie.title + "\t" + movie.kloStart +" - "+movie.kloEnd + " " +theatreName);
		}
		listViewMovies.setItems(obsList);
	}

	// Palauttaa kaikki tänään näytettävät
	private  ArrayList<Movie> findMovies(int id) {
		String timestamp = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
		try {
			return tr.getMovies(id, timestamp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	// Palauttaa kaikki tietyllä aikavälillä näytettävät
	private ArrayList<Movie> findMovies(int id, String pvm, String start, String end) {
		try {
			return tr.getMovies(id, pvm, start, end);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			 e.printStackTrace();
			 return null;
		}
	}

	private void fillCombo(ArrayList<Finnkino> arrlist) {
		// ArrayList<Finnkino> listOfTeatterit = Main.teatterit;
		for (Finnkino teatteri : arrlist) {
			comboTeatteri.getItems().addAll(teatteri.name + ": " + teatteri.id);
		}
	}

}