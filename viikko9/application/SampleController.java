package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.*;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

public class SampleController implements Initializable {
	@FXML
	ComboBox<String> comboTeatteri = new ComboBox();
	@FXML
	TextField fieldEsitysPaiva = new TextField();
	@FXML
	TextField fieldAAika = new TextField(); 
	@FXML
	TextField fieldPAika = new TextField(); 
	@FXML
	TextField fieldAnnaNimi = new TextField(); 
	@FXML
	Button buttonListaaElokuvat;
	@FXML
	Button buttonNimiHaku;
	

	
	@FXML
	private void handleButtonAction(ActionEvent event) {
		
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		fillCombo();
		
	}
	
	private void fillCombo() {
		ArrayList<Finnkino> listOfTeatterit = Main.teatterit;
		for (Finnkino teatteri : listOfTeatterit) {
			comboTeatteri.getItems().addAll(teatteri.kaupunki+": "+teatteri.teatterinNimi);
		}
	}

	
}