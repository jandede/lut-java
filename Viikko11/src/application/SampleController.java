package application;

import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class SampleController {
	ShapeHandler sh;

	@FXML
	ImageView mapImgView;
	@FXML
	Pane root;
	int pointRadius = 5;

	public void initialize() {
		sh = ShapeHandler.getInstance();
	}

	@FXML
	private void mouseClickMap(MouseEvent e) {
		// Eli tehdään pallot niin että klikkauskohdassa on keskipiste, helpompi
		// käsitellä
		double[] mPos = { e.getX() - pointRadius, e.getY() - pointRadius };
		for (Point p : sh.points) {
			// Ei tarvitse painaa palloa keskipisteestä(mahdotonta) vaan sitä voi painaa
			// koko pinta-alalta
			// TODO tästä suurin osa on turhaa
			if (Math.abs(p.locationXY[0] - mPos[0]) <= pointRadius
					&& Math.abs(p.locationXY[1] - mPos[1]) <= pointRadius) {
				//sh.linePoints.add(p);
				sh.pointClicked(p);
				return;
			}
		}
		sh.drawPoint(new Point(mPos, root, pointRadius), root, pointRadius);

	}

}
