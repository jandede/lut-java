package application;


import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.web.WebView;

import javafx.scene.control.TextField;

import java.util.ArrayList;

import javafx.event.ActionEvent;

public class SampleController {
	@FXML
	private TextField urlBox;
	@FXML
	private Button loadButton;
	@FXML
	private Button refreshButton;
	@FXML
	private WebView web;
	@FXML
	private Button shoutOutButton;
	@FXML
	private Button initializeButton;
	@FXML
	private Button previousButton;
	@FXML
	private Button nextButton;
	public void initialize() {		
		web.getEngine().load("https://www.lut.fi/");
	}
	
}