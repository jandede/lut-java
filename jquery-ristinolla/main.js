
$(document).ready(function(){
	alusta();
	return false;
});

var ruudukko=[];

var pelaaja;
var tableCell;
var ohje;

var nimi1;
var nimi2;

const tableIds = ["i0","i1","i2","i3","i4","i5","i6","i7","i8"];

const alusta = () =>{
		$("nimi-text-box1").val("");
		$("nimi-text-box2").val("");

		for(let i=0;i<=8;i++){
			ruudukko[i]=0;
			tableCell = document.getElementById(tableIds[i]).innerHTML = 0;

		}

		$("#dialogi").dialog({
			closeOnEscape: false,
			'title': "Nimivalinta",
			'buttons': {
				'Tallenna': function(event){

					var nimet = [];
					var virheet = [];
					nimet.push($("#nimi-text-box1").val());
					nimet.push($("#nimi-text-box2").val());

				  for(let i = 0; i<2;i++){
							if(nimet[i].length>8){
								virheet.push("Yli 8 kirjainta: pelaaja "+(i+1));
							}
							if(hasNumbers(nimet[i])){
								virheet.push("Numeroita: pelaaja "+(i+1));
							}
							for(let j=0;j<nimet[i].length;j++){
								if (nimet[i][j]==" "){
									virheet.push("Välilyöntejä: pelaaja "+(i+1));
								}

							}

					}

					if (virheet.length==0){
						$(this).closest(".ui-dialog").dialog('close');
					}	else {

						//poista kopiot
						var virhe = [];
						$.each(virheet, function(i, el){
							if($.inArray(el, virhe) === -1) virhe.push(el);
						});
							var virheteksti="";
							for(let i=0;i<virhe.length;i++){
								virheteksti += virhe[i]+"<br>"
							}

							$("#virheteksti").html("Nimet sisältävät seuraavia virheitä:<br>"
											+ virheteksti);

					}

				}
			}

		}).dialog('open');


		document.getElementById("huomautus").innerHTML = "&nbsp;"
		pelaaja=1;
		ohje = document.getElementById("ohje").innerHTML = ("Pelaajan "+pelaaja+" vuoro");
	}

	const tarkistus = (pelaaja) =>{
		var retval = false;

		function voitto(pelaaja){
			alert(('Pelaaja '+pelaaja+' voitti pelin!'));
			alusta();
			retval = true;
		}

	    // tarkastaa onko pystysuorassa suora
	    for(let i=0;i<3;i++){
				if(ruudukko[i]!=0 && ruudukko[i+3]==ruudukko[i] && ruudukko[i+6]==ruudukko[i]){
		    voitto(pelaaja);

				}
	    }

	    // vaakasuorassa
	    for(let i=0;i<7;i=i+3){
		if(ruudukko[i]!=0 && ruudukko[i+1]==ruudukko[i] && ruudukko[i+2]==ruudukko[i]){
			voitto(pelaaja);
				}
	    }

	    // vinossa
	    if(ruudukko[0]!=0 && ruudukko[0]==ruudukko[4] && ruudukko[0]==ruudukko[8]){
	    	voitto(pelaaja);
	    }

	    if(ruudukko[2]!=0 && ruudukko[2]==ruudukko[4] && ruudukko[2]==ruudukko[6]){
	    	voitto(pelaaja);
	    }

	    return retval;
	}
//tarkasta onko merkkijonossa numeroita
function hasNumbers(t){
			var regex = /\d/g;
			return regex.test(t);
		}

	const siirto = (paikka) =>{

		// Arvolla "&nbsp;" varaa "tyhjää tilaa" jota muutetaan tarvittaessa
		document.getElementById("huomautus").innerHTML = "&nbsp;"

		if(ruudukko[paikka]==0){

			ruudukko[paikka]=pelaaja;

			// ruudukon solu
			var tableCell = document.getElementById(tableIds[paikka]).innerHTML = pelaaja;

			//onnistunut siirto jos tarkistus(pelaaja)==true
			//eli pelaaja on valinnut siirron ruutuun jossa on 0
			if(!tarkistus(pelaaja)){
				if(pelaaja==1)
					pelaaja=2;
				else
					pelaaja=1;

				ohje = document.getElementById("ohje").innerHTML = ("Pelaajan "+pelaaja+" vuoro");

			}
		} else {
			document.getElementById("huomautus").innerHTML = "Valitse tyhjä ruutu!"
		}

	}
