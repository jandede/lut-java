import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Weather {
	private Document doc;
	private HashMap<String, String> hmap;

	public HashMap<String, String> getHmap() {
		return hmap;
	}
	public Weather(String content) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			doc = dBuilder.parse(new InputSource(new StringReader(content)));

			doc.getDocumentElement().normalize();
			
			hmap = new HashMap();
			parseCurrentData();

		} catch (ParserConfigurationException | SAXException | IOException ex) {
			// TODO Auto-generated catch block

		}
	}
	//current = xml tag
	private void parseCurrentData() {
		NodeList nodes = doc.getElementsByTagName("current");
		for(int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;
			
			hmap.put("Weather", getValue("weather", e, "value"));
		}
		
	}
	private String getValue(String tag, Element e, String attr) {
		return ((Element)e.getElementsByTagName(tag).item(0)).getAttribute(attr);
		
	}
}
