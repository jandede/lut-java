package viope1;

import java.util.Arrays;

public class Dog {
	String nimi;
	String lause;
	
	public Dog(String name) {
		System.out.println("Hei, nimeni on "+name);
		//System.out.println(name);
		nimi=name;
		
	}
	
	public void speak(String lause) {
		//System.out.println(nimi+": "+lause);
		String[] sanat = lause.split(" ");
		for(String sana : sanat) {
			if(sana!="")
				System.out.println(luku(sana));
		}
		
	}
	
	public String luku(String sana) {
		if(sana.equals("true") || sana.equals("false"))
			return "Such boolean: "+sana;
		else if(isNumeric(sana)) 
			return "Such integer: "+sana;
		else 
			return sana; 
		
		
	}
	public boolean isNumeric(String s) {  
	    return s != null && s.matches("[-+]?\\d*\\.?\\d+");  
	}  
}


