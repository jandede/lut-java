package viope1;


import java.util.Scanner;

public class Mainclass {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Anna koiralle nimi: ");
		String name = scanner.nextLine();
		
		//tarkasta onko Stringin pituus 0
		//trim() poista välilyönnit alussa ja lopussa
		if((name.trim()).isEmpty()) 
			name="Doge";
		Dog dog = new Dog(name);
		
		System.out.print("Mitä koira sanoo: ");
		String says = scanner.nextLine();
		if ((says.trim()).isEmpty())
			says="Much wow!";
		dog.speak(says);
		
		
	}

}
