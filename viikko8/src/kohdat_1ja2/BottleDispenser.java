
import java.awt.List;
import java.util.ArrayList;
import java.util.Locale;

public class BottleDispenser {	
	private int bottles;
    private double money;
    ArrayList<Bottle> bottles_list;
    
    private static BottleDispenser dispenser = null;
	
    
    private BottleDispenser() {
        bottles = 50;
        money = 0;
       
    }
    
    public void fillDispenser(ArrayList<Bottle> bottles_list) {
    	this.bottles_list = bottles_list;
    }
    
    public static BottleDispenser getInstance() {
    	if (dispenser == null)
    		dispenser = new BottleDispenser(); 
    	
    	return dispenser;
    	
    }
    
    public ArrayList<Bottle> buyBottle(int i) {
    	bottles_list.remove(i-1);
    	return bottles_list;
    	
    }
    
    
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    
    public void returnMoney() {
        System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€",money);

        money = 0;
    }
    
    public void listBottles() {
    	for(int i=0;i<bottles_list.size();i++) {
    		String koko = String.valueOf(bottles_list.get(i).koko).replace(',', '.');
    		String hinta = String.valueOf(bottles_list.get(i).hinta).replace(',', '.');
    		System.out.printf("%d. Nimi: %s\n\tKoko: %s\tHinta: %s\n", i+1,bottles_list.get(i).nimi,koko,hinta);

    	}
    }
}
