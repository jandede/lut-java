package application;

import applicationBottles.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class SampleController implements Initializable {
	ArrayList<Bottle> bottles_list;
	BottleDispenser bd;
	double money;

	@FXML
	private Label listOfBottles;
	@FXML
	private Button addMoney;
	@FXML
	private Label labelRahaa;

	@FXML
	private void handleButtonAction(ActionEvent event) {
		if(event.getSource() == addMoney) {
			money = bd.addMoney();
			labelRahaa.setText(""+money);
			
		}
		else {
			money = bd.returnMoney();
			labelRahaa.setText(""+money);
		}

	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		addBottles();
		money = bd.getMoney();
		labelRahaa.setText(""+money);
		listBottles();
		
	}
	
	private void addMoney() {
		money = bd.addMoney();
	}
	
	private void addBottles() {
		bottles_list = new ArrayList<Bottle>();

		Bottle pepsi05 = new Bottle("Pepsi Max", 0.5, 1.8);
		bottles_list.add(pepsi05);

		Bottle pepsi15 = new Bottle("Pepsi Max", 1.5, 2.2);
		bottles_list.add(pepsi15);

		Bottle cocaz05 = new Bottle("Coca-Cola Zero", 0.5, 2);
		bottles_list.add(cocaz05);

		Bottle cocaz15 = new Bottle("Coca-Cola Zero", 1.5, 2.5);
		bottles_list.add(cocaz15);

		Bottle fantaz05 = new Bottle("Fanta Zero", 0.5, 1.95);
		bottles_list.add(fantaz05);
		bottles_list.add(fantaz05);

		bd = BottleDispenser.getInstance();
		bd.fillDispenser(bottles_list);
		
	}
	private void listBottles() {
		String listOfBottlesString = "";
		for (Bottle bottle : bottles_list) {
			listOfBottlesString = listOfBottlesString + bottle.nimi + "\n";
			
		}
		listOfBottles.setText(listOfBottlesString);
		
		
	}

}
