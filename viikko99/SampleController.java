
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.*;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

public class SampleController implements Initializable {
	BottleDispenser bd;

	@FXML
	private Label listOfBottles;
	@FXML
	private Button addMoney;
	@FXML
	private Button returnMoney;
	@FXML
	private Label labelRahaa;
	@FXML
	private Slider rahaSlider = new Slider();
	@FXML
	private Label labelSlider;
	@FXML
	private Label labelViestiKentta;
	@FXML
	private ComboBox<String> comboBottles = new ComboBox();

	int sliderVal;

	@FXML
	private void handleButtonAction(ActionEvent event) {
		if (event.getSource() == addMoney) {
			if (sliderVal != 0)
				addMoney(sliderVal);
			setLabelRahaa();
			rahaSlider.setValue(0);

		} else if (event.getSource() == returnMoney) {
			labelViestiKentta.setText("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + moneyLeft() + " euroa.");
			bd.returnMoney();
			setLabelRahaa();
		}
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		addBottles();
		setLabelRahaa();
		listBottles();

		// Kuuntelee slideriä jatkuvasti
		rahaSlider.valueProperty().addListener((obs, oldValue, newValue) -> {
			sliderVal = (int) (Math.round(newValue.doubleValue()) / 10.0 * 10);
			labelSlider.setText("" + sliderVal);
		});

		comboBottles.valueProperty().addListener((obs, oldValue, newValue) -> {
			//String[] newValueArr = newValue.toString().split(" ");
			ArrayList<String> string = new ArrayList<String>(Arrays.asList(newValue.toString().split(" ")));
			double koko = Double.parseDouble(string.get(string.size() - 1));
			string.remove(string.size() - 1);
			String tuote = String.join(" ", string);
			Bottle ostaBottle = new Bottle(tuote, koko);
			if (bd.buyBottle(ostaBottle) == 0) {
				labelViestiKentta.setText(ostaBottle.nimi + " ostettu");
				setLabelRahaa();
			} else if (bd.buyBottle(ostaBottle) == 1) {
				labelViestiKentta.setText("Tuote ei ole saatavilla");

			} else if (bd.buyBottle(ostaBottle) == 2) {
				labelViestiKentta.setText("Lisää rahaa ostakseesi tuotteen!");
			//return 3: Kaikki pullot ostettu.
			} else {
				labelViestiKentta.setText("Pullokone on tyhjä.");
			}
		});
	}
	
	private void setLabelRahaa() {
		labelRahaa.setText("Credits: " + moneyLeft() + "€");
	}
	
	private void addMoney(int lisaaRahaa) {
		labelViestiKentta.setText("Klink! Lisää rahaa laitteeseen!");
		bd.addMoney(lisaaRahaa);
	}

	private void addBottles() {
		// Tähän muuttujaan listataan pullot ja kutsutaan BottleDispenser luokan metodia(koska pullot "luodaan" tässä pääluokassa)
		ArrayList<Bottle> bottles_list = new ArrayList<Bottle>();

		Bottle pepsi05 = new Bottle("Pepsi Max", 0.5, 1.8);
		bottles_list.add(pepsi05);

		Bottle pepsi15 = new Bottle("Pepsi Max", 1.5, 2.2);
		bottles_list.add(pepsi15);

		Bottle cocaz05 = new Bottle("Coca-Cola Zero", 0.5, 2);
		bottles_list.add(cocaz05);

		Bottle cocaz15 = new Bottle("Coca-Cola Zero", 1.5, 2.5);
		bottles_list.add(cocaz15);

		Bottle fantaz05 = new Bottle("Fanta Zero", 0.5, 1.95);
		bottles_list.add(fantaz05);
		bottles_list.add(fantaz05);

		// Luodaan singleton
		bd = BottleDispenser.getInstance();
		bd.fillDispenser(bottles_list);
	}

	private double moneyLeft() {
		// Pyöristä kahden numeron tarkkuudelle. Tämä numero ei ole mikään ratkaiseva
		// muuttuja vaan sitä
		// käytetään vain rahan näyttämiseen ruudulla.
		return Math.round(bd.getMoney() * 100.0) / 100.0;
	}

	private void listBottles() {
		ArrayList<String> listOfBottles = bd.listOfBottles();
		for (int i = 0; i < listOfBottles.size(); i++) {
			comboBottles.getItems().addAll(listOfBottles.get(i));
		}
	}
}
