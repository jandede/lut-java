
import java.util.ArrayList;

public class Bottle {
    public String nimi;
    double koko;
    double hinta;
    
    public Bottle() {
		//addBottles();
    }
	
	public Bottle(String name, double koko, double hinta) {
		this.nimi = name;
		this.koko = koko;
		this.hinta = hinta;
	}
	public Bottle(String name, double koko) {
		this.nimi = name;
		this.koko = koko;
	}
}
