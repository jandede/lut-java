package application;

import javafx.scene.layout.Pane;

public class Point {
	double[] locationXY;
	// Tallennetaan säde tänne koska se on sinäns pallon ominaisuus, vaikka se onkin kaikilla sama
	int pointRadius;
	public Point(double[] locationXY, Pane pane, int pointRadius) {
		this.locationXY = locationXY;
		this.pointRadius = pointRadius;
	}
}
