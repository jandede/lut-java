package application;

import java.util.ArrayList;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class ShapeHandler {
	Pane pane;
	static ShapeHandler sh = null;
	ArrayList<Point> points = new ArrayList<Point>();
	ArrayList<Point> linePoints = new ArrayList<Point>();
	ArrayList<ArrayList<Point>> lines = new ArrayList<ArrayList<Point>>();

	// Singleton
	public static ShapeHandler getInstance() {
		if (sh == null)
			sh = new ShapeHandler();
		return sh;
	}

	// SampleControllerin kutsuma funktio(painetaan olemassa olevaa palloa)
	public void pointClicked(Point point) {
		linePoints.add(point);
		// Jos on ensimmäinen klikkaus olemassa olevaan palloon, ei tehdä mitään. Jos ei
		// ole ensimmäinen, piirretään viiva.
		if (linePoints.size() > 1)
			drawLine();
		System.out.println("Hei, olen piste!");
	}

	public void drawPoint(Point point, Pane pane, int pointRadius) {
		// Tallennetaan pane koska sitä tarvitaan drawLine():ssä
		this.pane = pane;
		points.add(point);
		Circle circle;
		circle = new Circle(pointRadius, Color.BLACK);
		circle.relocate(point.locationXY[0], point.locationXY[1]);

		pane.getChildren().add(circle);

		linePoints.clear();
	}

	private void drawLine() {
		int sz = linePoints.size();
		// pointRadius on kaikilla palloilla sama, joten ei ole väliä mistä objektista se otetaan talteen.
		int pointRadius = linePoints.get(sz - 1).pointRadius;
		Line line = new Line();
		line.setStartX(linePoints.get(sz - 1).locationXY[0] + pointRadius); // jotta viiva lähtisi pallon keskipisteestä
		line.setStartY(linePoints.get(sz - 1).locationXY[1] + pointRadius);
		line.setEndX(linePoints.get(sz - 2).locationXY[0] + pointRadius);
		line.setEndY(linePoints.get(sz - 2).locationXY[1] + pointRadius);

		// lineAL on "yksikkö" kaksi uloitteiseen lines listaan
		ArrayList<Point> lineAL = new ArrayList<Point>();
		// Siihen lisätään "viiva", eli kaksi pistettä
		lineAL.add(linePoints.get(sz - 1));
		lineAL.add(linePoints.get(sz - 2));
		// Viiva lisätään lines listaan
		lines.add(lineAL);

		pane.getChildren().add(line);

	}

}
