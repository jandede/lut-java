import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Mainclass {
	static String zip_file = "zipinput.zip";
	static String in= "input.txt";
	static String out = "output.txt";
	
	public static void main(String[] args) throws IOException {
		//String userdir = System.getProperty("user.dir");
		//String in = userdir + "/src/input.txt";
		//readFile(in);
		//String out = userdir + "/src/output.txt";
		
		
		readAndWrite(in, out);

	}

	static void readFile(String sijainti) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(sijainti));

		try {
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		in.close();

	}

	static void readAndWrite(String input, String output) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(input));
		BufferedWriter bw = new BufferedWriter(new FileWriter(output));

		try {
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if(inputLine.length() < 30 && !inputLine.equals("") && !inputLine.equals(" ") && !inputLine.equals("  ")) {
					for(int i=0;i<inputLine.length();i++) {
						if(inputLine.charAt(i)=='v') {
							bw.write(inputLine+"\n");
							break;
						}
					}
				}
			}
			in.close();
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	static void pura(String tiedosto) {
		byte[] buffer = new byte[1024];
		
	}

}
