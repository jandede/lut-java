
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class BottleDispenser {
	private double money;
	ArrayList<Bottle> bottles_list;
	ArrayList<Bottle> purchased_bottles = new ArrayList<Bottle>();

	private String tuotteet[] = { "Pepsi Max", "Coca-Cola Zero", "Fanta Zero" };
	private double pulloKoot[] = { 0.5, 1.5 };

	// Singleton
	private static BottleDispenser dispenser = null;

	// return 0 : tuote löytyy valikoimasta ja pullo ostetaan
	// return 1 : tuotetta ei ole valikoimassa
	// return 2 : Löytyy mutta rahaa ei ole tarpeeksi
	// return 3 : pullokone on tyhjä, ei voi ostaa enää mitään
	public int buyBottle(Bottle bottle) {
		if (bottles_list.size() > 0) {
			for (int i = 0; i < bottles_list.size(); i++) {
				if (bottles_list.get(i).nimi.equals(bottle.nimi) && bottles_list.get(i).koko == bottle.koko) {
					if (money >= bottles_list.get(i).hinta) {
						purchased_bottles.add(bottles_list.get(i));
						money -= bottles_list.get(i).hinta;
						bottles_list.remove(i);
						return 0;
					} else {
						return 2;
					}
				}
			}
			return 1;
		}
		return 3;
	}

	private BottleDispenser() {
		money = 0;
	}

	public void fillDispenser(ArrayList<Bottle> bottles_list) {
		this.bottles_list = bottles_list;
	}

	// Singletonin käyttöönotto
	public static BottleDispenser getInstance() {
		if (dispenser == null)
			dispenser = new BottleDispenser();
		return dispenser;
	}

	public ArrayList<Bottle> buyBottle(int i) {
		bottles_list.remove(i - 1);
		return bottles_list;
	}

	public ArrayList<Bottle> getBottles() {
		return bottles_list;
	}

	public double addMoney(double lisaaRahaa) {
		money += lisaaRahaa;
		return money;
	}

	// Käytetään tässäkin return doublea koska rahat ovat tässä ohjelmassa muutenkin
	// doubleja(vaikka voisi vain palauttaa nollan).
	public double returnMoney() {
		printReceipt(money);
		money = 0;
		return money;
	}
	
	// Käydään läpi kaikki mahdolliset tuotevaihtoehdot ja tehdään niistä ArrayList 
	//(merkki,pullokoko) joka voidaan näyttää UI:ssa(ComboBox).
	public ArrayList<String> listOfBottles() {
		ArrayList<String> bottlesForCombo = new ArrayList<String>();
		for (String tuote : tuotteet) {
			for (double d : pulloKoot) {
				bottlesForCombo.add(tuote + " " + d);
			}
		}
		return bottlesForCombo;
	}

	// Jäljellä olevalle rahalle tulostin koska sitä tarvitaan useamman kerran kontrollerissa.
	public double getMoney() {
		return money;
	}

	public void printReceipt(double money) {
		String receipt = "Kuitti pulloautomaatista:";
		double total = 0;
		for (Bottle bottle : purchased_bottles) {
			// Tulee vähän ikävän näköinen kuitti kun ostaa Coca Cola Zeroja...
			receipt = receipt + String.format("\n%.1f %s\t\t\t%.2f€", bottle.koko, bottle.nimi, bottle.hinta);
			total += bottle.hinta;
		}
		receipt = receipt
				+ String.format("\nYhteensä: %.2f€\nRahaa takaisin: %.2f€\n\nTervetuloa uudestaan.", total, money);
		printToFile(receipt);
	}

	public void printToFile(String receipt) {
		try {
			FileWriter fw = new FileWriter("kuitti.txt");
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write(receipt);
			bw.close();

		} catch (IOException e) {
			System.out.println("VIRHE: " + e);

		}
	}
}
