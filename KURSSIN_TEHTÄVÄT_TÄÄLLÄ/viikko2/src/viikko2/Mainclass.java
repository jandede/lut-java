package viikko2;

import java.util.Scanner;

public class Mainclass {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Anna koiralle nimi: ");
		String name = scanner.nextLine();
		System.out.print("Koira sanoo : ");
		String says = scanner.nextLine();
		
		//tarkasta onko Stringin pituus 0
		//trim() poista välilyönnit alussa ja lopussa
		if((name.trim()).isEmpty()) 
			name="Doge";
		if ((says.trim()).isEmpty())
			says="Much wow!";
		
		Dog dog = new Dog(name);
		dog.lause=says;
		dog.speak(says);
		
	}

}
