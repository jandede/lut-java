
import java.util.Arrays;

public class Movie {
    String title;
    String kloStart;
    String kloEnd;

    // Mikäli etsitään elokuvaa kaikista teattereista
    int idOfTheatre;

    public Movie(String title, String kloStart, String kloEnd) {
        //klo muotoa:
        //2017-11-19T21:30:00
        String[] dts = kloStart.split("T");
        this.kloStart = dts[1];
        this.title = title;
        String[] dte = kloEnd.split("T");
        this.kloEnd = dte[1];

    }
    public Movie(String title, String kloStart, String kloEnd, int idOfTheatre) {
        //klo muotoa:
        //2017-11-19T21:30:00
        String[] dts = kloStart.split("T");
        this.kloStart = dts[1];
        this.title = title;
        String[] dte = kloEnd.split("T");
        this.kloEnd = dte[1];
        this.idOfTheatre = idOfTheatre;

    }


}