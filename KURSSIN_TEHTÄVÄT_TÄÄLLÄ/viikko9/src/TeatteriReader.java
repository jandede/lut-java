

import java.net.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.*;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class TeatteriReader {
    private Document doc;
    private HashMap<String, String> hmap;
    static TeatteriReader tr;

    // Tätä muuttujaa käytetään kahdessa kohtaa, joten sitä on turha parsia useampaan kertaan koska sen tiedot eivät muutu.
    private ArrayList<Finnkino> arlTheatres = null;

    public ArrayList<Movie>findAMovie(String movie) {
        String timestamp = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
        ArrayList<Movie> moviesFound = new ArrayList<Movie>();
        for (int i = 0; i < arlTheatres.size(); i++) {
            try {
                ArrayList<Movie> moviesInTheatre = getMovies(arlTheatres.get(i).id, timestamp);

                for(Movie mov : moviesInTheatre) {
                    if (mov.title.equals(movie)) {
                        moviesFound.add(mov);
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return moviesFound;

    }

    public ArrayList<Movie> getMovies(int id, String pvm) throws IOException {
        // http://www.finnkino.fi/xml/Schedule/?area=<teatterinID>&dt=<päivämäärä
        // pp.kk.vvvv>
        URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + pvm);

        String line;
        String content = "";

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        while ((line = br.readLine()) != null)
            content += line;

        return getMovies(content,id);
    }

    private int convertToS(String time) {
        String[] units = time.split(":");
        int duration;
        try {
            int hours = Integer.parseInt(units[0]);
            int minutes = Integer.parseInt(units[1]);
            duration = 3600 * hours + 60 * minutes;
        } catch (NumberFormatException e) {
            // duration = 3600 * Integer.parseInt(units[0]);
            duration = 0;
        }
        // System.out.println(duration);
        if(duration >= 100000)
            System.out.println(duration + " " + time);
        return duration;
    }

    public ArrayList<Movie> getMovies(int id, String pvm, String start, String end) throws IOException {
        URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + pvm);

        // XML:ssä muoto on hh:mm

        String line;
        String content = "";

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        while ((line = br.readLine()) != null)
            content += line;

        ArrayList<Movie> alm = getMovies(content, id);
        ArrayList<Movie> almCopy = new ArrayList<Movie>();
        //System.out.println(start);

        for (Movie movie : alm) {
            if (convertToS(movie.kloStart) >= convertToS(start) && convertToS(movie.kloEnd) <= convertToS(end)) {
                almCopy.add(movie);
            }
        }


        // return getMovies(content);
        return almCopy;

        // TODO Eliminate items not in time range

    }

    public static TeatteriReader getInstance() {
        if (tr == null)
            tr = new TeatteriReader();
        return tr;
    }

    public ArrayList<Finnkino> getXMLTheatre() throws IOException {
        String line;
        String content = "";

        URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        while ((line = br.readLine()) != null)
            content += line;

        return getTheatres(content);
    }

    private ArrayList<Movie> getMovies(String content, int id) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(new InputSource(new StringReader(content)));

            doc.getDocumentElement().normalize();

            return parseCurrentDataMovie(id);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    private ArrayList<Finnkino> getTheatres(String content) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(new InputSource(new StringReader(content)));

            doc.getDocumentElement().normalize();

            return parseCurrentDataTheatre();

        } catch (ParserConfigurationException | SAXException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    private ArrayList<Finnkino> parseCurrentDataTheatre() {
        arlTheatres = new ArrayList<Finnkino>();
        NodeList nodes;
        ArrayList<Finnkino> arrayListTheatre = new ArrayList<Finnkino>();
        nodes = doc.getElementsByTagName("TheatreArea");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;

            arrayListTheatre.add(new Finnkino(Integer.parseInt(getValue("ID", e)), getValue("Name", e)));
        }
        // Joka kerta kun tämä funktio ajetaan olisi hyvä päivittää tieto luokkamuuttujaan:
        arlTheatres = arrayListTheatre;
        return arrayListTheatre;

    }

    private ArrayList<Movie> parseCurrentDataMovie(int id) {
        NodeList nodes;
        ArrayList<Movie> arrayListMovie = new ArrayList<Movie>();
        nodes = doc.getElementsByTagName("Show");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;

            arrayListMovie
                    .add(new Movie(getValue("Title", e), getValue("dttmShowStart", e), getValue("dttmShowEnd", e), id));
        }
        return arrayListMovie;
    }

    private String getValue(String tag, Element e) {
        String str = ((Element) e.getElementsByTagName(tag).item(0)).getTextContent();
        return str;
    }

}