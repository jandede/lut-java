
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

public class Main extends Application {
    public static ArrayList<Finnkino> teatterit = new ArrayList<Finnkino>();
    /*
     * @Override public void start(Stage primaryStage) {
     *
     * //Parent root = FXMLLoader.load(getClass().getResource("Sample.fxml"));
     *
     * //Scene scene = new Scene(root);
     *
     * //stage.setScene(scene); //stage.show();
     *
     * try { Parent root = FXMLLoader.load(getClass().getResource("/Sample.fxml"));
     * Scene scene = new Scene(root); primaryStage.setScene(scene);
     * primaryStage.show(); } catch(Exception e) { e.printStackTrace(); }
     */

    @Override
    public void start(Stage stage){
        AnchorPane root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("Sample.fxml"));
            //AnchorPane root = (AnchorPane) FXMLLoader.load(Main.class.getResource("/packagename/LoginGUI.fxml"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }



    public static void main(String[] args) {



        //tr.getTeatterit("areas.xml");
		/*
		teatterit.add(new Finnkino("Espoo", "Omena"));
		teatterit.add(new Finnkino("Espoo", "Sello"));
		teatterit.add(new Finnkino("Helsinki", "Kinopalatsi"));
		teatterit.add(new Finnkino("Helsinki", "Tennispalatsi"));
		teatterit.add(new Finnkino("Jyväskylä", "Fantasia"));
		teatterit.add(new Finnkino("Kuopio", "Scala"));
		teatterit.add(new Finnkino("Lahti", "Kuvapalatsi"));
		teatterit.add(new Finnkino("Lappeenranta", "Strand"));
		teatterit.add(new Finnkino("Oulu", "Plaza"));
		teatterit.add(new Finnkino("Pori", "Promenadi"));
		teatterit.add(new Finnkino("Tampere", "Cine Atlas"));
		teatterit.add(new Finnkino("Tampere", "Plevna"));
		teatterit.add(new Finnkino("Turku", "Kinopalatsi"));
		teatterit.add(new Finnkino("Vantaa", "Flamingo"));
		*/

        launch(args);
    }
}