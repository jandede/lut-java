package application;

import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.web.WebView;

import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import javafx.event.ActionEvent;

public class SampleController {
	@FXML
	private WebView web;
	@FXML
	private TextField addressBar;
	@FXML
	private Button loadButton;
	@FXML
	private Button initializeButton;
	@FXML
	private Button shoutOutButton;
	@FXML
	private Button prevButton;
	@FXML
	private Button nextButton;
	private int historyInt = 10;

	private String lastPage = null, prevPage = null, currPage = null;

	private ArrayList<String> history = new ArrayList<String>();
	
	ListIterator<String> itr = history.listIterator();

	@FXML
	void mouseClickWeb() {
		if(!history.get(history.size()-1).equals(web.getEngine().getLocation()))
			addToHistory(web.getEngine().getLocation());
	}

	@FXML
	void onButtonClick(ActionEvent event) {
		if (event.getSource() == loadButton) {
			loadURL(addressBar.getText());
		} else if (event.getSource() == initializeButton) {
			if (addressBar.getText().equals("index.html")) {
				web.getEngine().executeScript("initialize()");
			}
		} else if (event.getSource() == shoutOutButton) {
			if (addressBar.getText().equals("index.html")) {
				web.getEngine().executeScript("document.shoutOut()");
			}
		} else if (event.getSource() == nextButton) {
			itr.next();
			if(itr.hasNext()) {
				loadURL(itr.next());
			}

		} else if (event.getSource() == prevButton) {
			itr.previous();
			if(itr.hasPrevious()) {
				try {
					loadURL(itr.previous());
				} catch(NoSuchElementException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void initialize() {
		/*
		prevButton.setDisable(true);
		nextButton.setDisable(true);*/
		loadURL("https://www.lut.fi");
	}
	
	private void addToHistory(String url) {
		// "Korjataan" url ja katsotaan onko se tämän hetkinen sivu. Jos on niin ei tehdä mitään.
		url = fixURL(url);
		if(history.size() != 0) {
			if(history.get(history.size()-1).equals(url)) 
				return;
		}
		/*
		prevButton.setDisable(!itr.hasPrevious());
		nextButton.setDisable(!itr.hasNext());
		*/
		addressBar.setText(url);
		
		if(history.size() >= 10)
			history.remove(0);
		history.add(url);
		System.out.println(history);
		
		//Pidetään iteraattori viimeisessä objektissa
		itr = history.listIterator(history.size());
	}
	
	
	// Lisätään https alkuun jos ei valmiiksi ole http tai https. Poistetaan lopussa oleva /-merkki. Muutetaan pieniksi kirjaimiksi.
	public String fixURL(String url) {
		url = url.toLowerCase();
		// index.html ei ole http-sivu vaan paikallinen tiedosto.
		if(url.equals("index.html"))
			return url;
		if (!url.substring(0, 8).equals("https://") && !url.substring(0, 7).equals("http://")) {
			url = "https://" + url;
		} 
		if (url.substring(url.length()-1).equals("/"))
			url = url.substring(0, url.length() - 1);
		return url;
	}

	private void loadURL(String url) {
		System.out.println("URL LOAD: " +url);
		url = fixURL(url);
		if(!history.contains(url))
			addToHistory(url);
		
		if (url.equals("index.html")) {
			System.out.println("Loading index.html");
			web.getEngine().load(getClass().getResource("index.html").toExternalForm());
			shoutOutButton.setDisable(false);
			initializeButton.setDisable(false);
		} else {
			shoutOutButton.setDisable(true);
			initializeButton.setDisable(true);
			web.getEngine().load(url);
		}
		/*
		prevButton.setDisable(!itr.hasPrevious());
		nextButton.setDisable(!itr.hasNext());*/
		addressBar.setText(url);
	}

}