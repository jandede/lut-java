
import java.util.Scanner;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Mainclass {
	public static void main(String[] args) throws IOException {
		Scanner scanner = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Anna koiralle nimi: ");
		String name = scanner.next();
		Dog dog1 = new Dog(name);
		
		System.out.print("Mitä koira sanoo: ");
		String says = br.readLine();
		dog1.speak(says);
		
		
		//tarkasta onko Stringin pituus 0
		//trim() poista välilyönnit alussa ja lopussa
		/*String name1="Rekku", says1="Hau!";
		String name2="Musti", says2="Vuh!";*/
		/*if((name.trim()).isEmpty()) 
			name="Doge";
		if ((says.trim()).isEmpty())
			says="Much wow!";
		*/
		
		
	}

}
