import java.util.ArrayList;
import java.util.List;

public class Car {
	String merkki;
	String malli;
	String varustetaso;
	String tyyppi; //esim. B, C tai D
	
	static List<String> osat = new ArrayList<String>();
	
	public Car(String merkki, String malli, int renkaidenLkm) {
		this.merkki = merkki;
		this.malli = malli;

		new Alusta();
		osat.add("Body");
		new Kori();
		osat.add("Chassis");
		new Moottori();
		osat.add("Engine");
		int wheelLkm = 0;
		
		for(int i=0;i<renkaidenLkm;i++) {
			new Rengas();
			wheelLkm++;
		}
		osat.add("Wheel");
		
		System.out.println("Autoon kuuluu:");
		for(int i=0; i<osat.size(); i++) {
			if(osat.get(i).equals("Wheel"))
				System.out.println("\t"+String.valueOf(wheelLkm)+" Wheel");
			else
				System.out.println("\t" + osat.get(i));
		}
	}
	
}

class Alusta {
	double jousiJaykkyys;
	double iskariJaykkyys;
	double jarruteho;
	double jarrutyyppi;
	
	Alusta(){
		System.out.println("Valmistetaan: Body");
		
		
	}
	
}

class Vaihteisto {
	int vaihteidenMaara;
	int[] valitysSuhde;
	String tyyppi;
	Vaihteisto(){
		System.out.println("Valmistetaan: Transmission");
	}
	
	
}

class Moottori {
	double iskutilavuus;
	double vaanto;
	double teho;
	int kmAjettu;
	int sylinterit;
	String polttoaine;
	Moottori(){
		System.out.println("Valmistetaan: Engine");
	}
}

class Kori {
	String vari;
	double ilmanvastusK;
	
	public Kori() {
		System.out.println("Valmistetaan: Chassis");
		
	}
	
	public Kori(String vari) {
		System.out.println("Valmistetaan: Body");
		this.vari = vari;
	}
	
}

class Rengas {
	int halkaisija;
	int profiili;
	int leveys;
	int pulttijako;
	double paine;
	Rengas(){
		System.out.println("Valmistetaan: Wheel");
	}
	
}