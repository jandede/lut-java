package zip;

import java.io.*;
import java.util.*;
import java.util.zip.*;

public class Mainclass {
	public static void main(String[] args) {
		//System.out.println( System.getProperty( "user.dir" ) );
		String zipfile = "./zipinput.zip";
		String dest = "./";
		
		String path = unzip(zipfile,dest);
		//System.out.println(path);
		
		try {
			readOutputFile(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static String unzip(String zipfile, String destination) {
		File directory = new File(destination);
		
		if(!directory.exists())
			directory.mkdirs();
		
		FileInputStream fis;
		
		byte[] buffer = new byte[1024];
		File newFile = null;
		
		try {
			fis = new FileInputStream(zipfile);
			ZipInputStream zis = new ZipInputStream(fis);
			ZipEntry ze = zis.getNextEntry();
			
			while(ze != null) {
				String filename = ze.getName();
				newFile = new File(destination+ File.separator + filename);
				
				//System.out.println("Unzipping to "+ newFile.getAbsolutePath());
				
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);
				
				int length;
				
				while((length = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, length);
				}
				fos.close();
				
				zis.closeEntry();
				ze = zis.getNextEntry();
				//return newFile.getAbsolutePath();
			}
			zis.closeEntry();
            zis.close();
            fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return newFile.getAbsolutePath();
	}
	
	static void readOutputFile(String sijainti) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(sijainti));

		try {
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		in.close();

	}

}
