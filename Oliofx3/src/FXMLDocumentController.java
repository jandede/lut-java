
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField textInput = new TextField();
    //private Button button;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        //System.out.println(textInput.getText());
    	if (textInput.getText() != null)
    		label.setText(textInput.getText());
    	else
    		System.out.println("vittu");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
}
