package application;

import com.sun.prism.paint.Color;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MyApplication extends Application {

	@Override
	public void start(Stage primaryStage) {
		Button btn = new Button("VITTU");
		BorderPane p  = new BorderPane();
		Text t = new Text("JavaFX");
		Label label = new Label();
		t.setFont(Font.font("Arial", 60));
		t.setEffect(new DropShadow());
		p.setCenter(t);
		primaryStage.setScene(new Scene(p));
		primaryStage.show();
		
	}
	btn.setOnAction(new EventHandler<ActionEvent>()){
		@Override public void handle(ActionEvent e) {
	        label.setText("Accepted");
	    }
	});
	

	public static void main(String[] args) {
		launch(args);
	}
}
