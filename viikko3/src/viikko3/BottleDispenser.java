package viikko3;

import java.awt.List;
import java.util.ArrayList;
import java.util.Locale;

public class BottleDispenser {	
	private int bottles;
    private double money;
	
    ArrayList<Bottle> bottles_list = new ArrayList<Bottle>();
    
    public void fillDispenser() {
		Bottle pepsi05 = new Bottle("Pepsi Max",0.5,1.8);
		bottles_list.add(pepsi05);
	    
	    Bottle pepsi15 = new Bottle("Pepsi Max",1.5,2.2);
	    bottles_list.add(pepsi15);
	    
	    Bottle cocaz05 = new Bottle("Coca-Cola Zero",0.5,2);
	    bottles_list.add(cocaz05);
	    
	    Bottle cocaz15 = new Bottle("Coca-Cola Zero",1.5,2.5);
	    bottles_list.add(cocaz15);
	    
	    Bottle fantaz05 = new Bottle("Fanta Zero",0.5,1.95);
	    bottles_list.add(fantaz05);
	    bottles_list.add(fantaz05);
    }
    
	//ArrayList<Bottle> pullot;
    
    public BottleDispenser() {
        bottles = 50;
        money = 0;
        fillDispenser();
       
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle(int index) {
    	if(bottles>=1 && money >= bottles_list.get(index-1).hinta) {
    		bottles -= 1;
    		money -= bottles_list.get(index-1).hinta;
    		System.out.printf("KACHUNK! %s tipahti masiinasta!\n",bottles_list.get(index-1).nimi);
    		bottles_list.remove(index-1);
    	} else {
    		System.out.println("Syötä rahaa ensin!");
    	}
    
       
    }
    
    public void returnMoney() {
        System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€",money);

        money = 0;
    }
	//System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + String.format(Locale.GERMAN, "%.2f", money) + "€");
    
    public void listBottles() {
    	for(int i=0;i<bottles_list.size();i++) {
    		String koko = String.valueOf(bottles_list.get(i).koko).replace(',', '.');
    		String hinta = String.valueOf(bottles_list.get(i).hinta).replace(',', '.');
    		//System.out.printf("%d. Nimi: %s\n\tKoko: %.1f\tHinta: %.1f\n", i+1,bottles_list.get(i).nimi,koko,hinta);    		
    		System.out.printf("%d. Nimi: %s\n\tKoko: %s\tHinta: %s\n", i+1,bottles_list.get(i).nimi,koko,hinta);

    		//String.format(Locale.GERMAN, "%.2f",bottles_list.get(i).hinta)
    	}
    }
}
