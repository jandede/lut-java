package viikko3;

import java.util.ArrayList;
import java.util.Scanner;

public class Mainclass {
	public static void main(String[] args) {
		int valinta;
		boolean run = true;
		
		
		/*
		bottleDispenser.addMoney();
		bottleDispenser.buyBottle(pullo);
		
		bottleDispenser.buyBottle(pullo);
		bottleDispenser.addMoney();
		bottleDispenser.addMoney();
		bottleDispenser.buyBottle(pullo);
		
		bottleDispenser.returnMoney();
		*/
		
		//Bottle pullo = new Bottle();
		//ArrayList<Bottle> bottles_list = pullo.bottles_list;
		//System.out.println(bottles_list);
		
		BottleDispenser bottleDispenser = new BottleDispenser();

		while (run) {
			System.out.println("\n*** LIMSA-AUTOMAATTI ***\n1) Lisää rahaa koneeseen"
					+ "\n2) Osta pullo\n3) Ota rahat ulos\n4) Listaa koneessa olevat pullot"
					+ "\n0) Lopeta");
			
			Scanner scanner = new Scanner(System.in);
			System.out.print("Valintasi: ");
			
			valinta = scanner.nextInt();
			
			switch(valinta) {
				case 0: 
					run=false;
					break;
				case 1:
					bottleDispenser.addMoney();
					break;
				case 2:
					bottleDispenser.listBottles();
					System.out.print("Valintasi: ");
					int index = scanner.nextInt(); 
					bottleDispenser.buyBottle(index);
					break;
				case 3:
					bottleDispenser.returnMoney();
					break;
				case 4:
					bottleDispenser.listBottles();
					break;
				default:
					System.out.println("virhe");
			}
		}
	}

}
