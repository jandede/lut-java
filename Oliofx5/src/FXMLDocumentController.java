
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {

    @FXML
    private TextArea textArea = new TextArea();
    @FXML
    private TextField tiedostoInput = new TextField();
    @FXML 
    private Button lataaButton;
    @FXML 
    private Button tallennaButton;
    @FXML
    private Label errorLabel;
    @FXML
	Label realTimeText;

    @FXML
	Label label;
    @FXML
	Button addToLabelButton;
	@FXML
	Button addFromTextField;
    String hw = "Hello World";

	@FXML
	void handleButton(ActionEvent event) {
		if(event.getSource() == addToLabelButton) {
			System.out.println(hw);
			label.setText(hw);
		} else {
			label.setText(tiedostoInput.getText());
		}
	}

	@FXML
    private void evtListener(ActionEvent event) {
    	String file;
    	if(tiedostoInput.getText() != null) {
	    	if(event.getSource() == lataaButton) {
	    		file = readFile(tiedostoInput.getText());
	    		if (!file.equals("ERROR"))
	    			textArea.setText(file);
	    		else
	    			errorLabel.setText("error");
	    			
	    		
	    	}
	    	else if(event.getSource() == tallennaButton) {
	    		if(writeFile(tiedostoInput.getText(), textArea.getText()) == 0) {
	    			errorLabel.setText("Onnistui");
	    		} else {
	    			errorLabel.setText("error");
	    		}
	    		
	    	}
    	}
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
		textArea.textProperty().addListener((observable, oldValue, newValue) -> {
			realTimeText.setText(newValue);
		});
    	
    }    
    
    public String readFile(String filename) {
    	String line = null;
    	String fileString = "";
    	
    	try {
    		FileReader fileReader = new FileReader(filename);
    		
    		BufferedReader bufferedReader = new BufferedReader(fileReader);
    		while((line = bufferedReader.readLine()) != null) {
    			//System.out.println(line);
    			fileString = fileString + line + "\n";
    		}
    		bufferedReader.close();
    	} catch(IOException e) {
    		fileString = "ERROR";
    	}
    	return fileString;
    }
    
    public int writeFile(String fileName, String matsku) {
    	try {
    		FileWriter fw = new FileWriter(fileName);
    		BufferedWriter bw = new BufferedWriter(fw);
    		
    		bw.write(matsku);
    		bw.close();
    		return 0;
    		
    	} catch (IOException e) {
    		return 1;
    	}
    	
    	
    }
    
    
}
