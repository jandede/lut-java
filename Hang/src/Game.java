import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {
	List<String> base = new ArrayList<String>();
	List<Integer> correct_letters = new ArrayList<Integer>();
	String word;
	List<String> incorrect_letters = new ArrayList<String>();
	boolean game_won = false;

	public Game(String word_in) {
		for (int i = 0; i < word_in.length(); i++) {
			base.add("_");
		}

		word = word_in;
	}

	public List<Integer> ask() {
		Scanner sc = new Scanner(System.in);

		String letter = sc.nextLine();
		List<Integer> correct_index = new ArrayList<Integer>();
		
		for(int i=0;i<correct_letters.size();i++) {
			if(base.get(correct_letters.get(i)).equals(letter)) 
				//IF letter has already been used, return a special value of null
				//and exit the method
				return null;
			
		}
		for(int i=0;i<incorrect_letters.size();i++) {
			if(incorrect_letters.get(i).equals(letter))
				return null;
		}

		if (letter.length() == 1) {
			
			//this is to keep track if the if* block succeeds
			boolean correctAnswer = false;
			
			char letter_char = letter.charAt(0);
			for (int i = 0; i < word.length(); i++) {
				if (letter_char == word.charAt(i)) { //*
					correct_index.add(i);
					correct_letters.add(i);
					correctAnswer=true;
				}
				
				}
			if (!correctAnswer){ //*
				incorrect_letters.add(letter);
			}

			
		}

		return correct_index;

	}

	public void reveal(List<Integer> correct_index) {
		for (int i = 0; i < correct_index.size(); i++) {
			base.set(correct_index.get(i), word.charAt(correct_index.get(i)) + "");
			
		}
		if (correct_letters.size() == word.length()) {
			game_won = true;
		}
	}

}
