import java.util.List;
import java.util.Random;
import java.util.ArrayList;

public class Mainclass {
	public static void main(String[] args) {

		int difficulty;
		difficulty = 7;

		MakeWords make = new MakeWords(difficulty);

		String word = make.pickRandomWord(difficulty);
		System.out.println(word);

		int lives = difficulty;

		Game game = new Game(word);

		boolean Run = true;

		while (Run) {
			print_base(game.base, game.incorrect_letters);

			List<Integer> correct_index = game.ask();
			if (correct_index == null) {
				System.out.println("You already used this letter. Try something else.");
				continue;
			} else if (correct_index.size() == 0) {
				lives--;
				System.out.printf("You lost a life. %d lives remaining.\n", lives);

				if (lives == 0) {
					System.out.println("You lose. The word was " + word + ".");
					Run = false;
				}

			} else {
				game.reveal(correct_index);
				if (game.game_won) {
					game.incorrect_letters.clear();
					print_base(game.base, game.incorrect_letters);
					System.out.println("\tYou win");
					Run = false;
				}
			}

		}
	}

	public static void print_base(List<String> base, List<String> incorrect_letters) {
		for (int i = 0; i < base.size(); i++) {
			System.out.print(base.get(i) + " ");
		}
		if (incorrect_letters.size() > 0)
			System.out.print("\tYou have tried the following inncorrect letters: " + incorrect_letters);

	}

}
