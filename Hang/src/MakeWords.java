import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MakeWords {
	String[] wordsList;

	public MakeWords(int difficulty) {

		switch (difficulty) {

		case 4:
			String words4 = "want, rind, cads, cyst, agog, raga, buts, slap, ergo, dill, outs, nose, gaol, tier, dory, toll, slog, weed, bile, pate, toil, aria, lays, suns, clue, push, wasp, find, fill, buss";
			wordsList = words4.split(", ");
			break;

		case 5:
			String words5 = "detox, holed, slant, aurae, tamps, store, aloha, vibes, azure, semis, tines, vizor, tempo, dozes, peers, dying, until, gaily, diced, ounce, urned, forks, drabs, gales, golly, combs, titan, banes, snubs, slots";
			wordsList = words5.split(", ");
			break;

		case 6:
			String words6 = "hagged, hedged, fiscal, suntan, smarmy, herald, vessel, scuffs, pellet, snugly, bayous, drably, abhors, clings, midges, biopsy, fields, turner, swamps, glands, credos, salami, dashes, numbed, housed, bursts, states, deacon, vivace, manage";
			wordsList = words6.split(", ");
			break;

		case 7:
			String words7 = "inmates, tanking, unhooks, bobbles, oregano, scrimps, toxemia, slipper, ignored, matador, canines, sapling, jumbled, abrades, avenges, arguing, lookout, listing, husking, diploma, dolphin, rectors, impinge, plastic, mislaid, chalked, gluiest, plagues, uterine, outsell";
			wordsList = words7.split(", ");
			break;

		default:
			System.out.println("ERROR");

		}

	}

	public String pickRandomWord(int difficulty) {
		Random rand = new Random();
		int randomNumIndex = rand.nextInt(wordsList.length);

		return wordsList[randomNumIndex];

	}

}
